use super::*;
use std::rc::Rc;
use std::sync::RwLock;

#[derive(Debug)]
pub struct SimpleTree<'a, E: Ord + Eq + Sync>(
    RwLock<AnyNode<'a, E, SimpleInternalNode<'a, E, Leaves<E>>, Leaves<E>>>,
);

#[derive(Debug)]
pub struct Leaves<T: Ord + Eq>(Vec<Rc<T>>, usize);

#[derive(Debug)]
pub struct SimpleInternalNode<'a, E: Ord + Eq, L: 'a + Leaf<'a, E>> {
    children: Vec<AnyNode<'a, E, Self, Leaves<E>>>,
    last: Option<Rc<E>>,
    max_size: usize,
    _pda: PhantomData<E>,
    _pdb: PhantomData<L>,
}

impl<'a, T: Ord + Eq + Sync> Tree<'a, T> for SimpleTree<'a, T> {
    fn empty(max: usize) -> Self {
        Self(RwLock::new(AnyNode::Leaf(Leaves::empty(max))))
    }

    fn search(&self, target: &T) -> Option<Rc<T>> {
        let s = self.0.read().unwrap();
        match &*s {
            AnyNode::Leaf(l) => l.search(target),
            AnyNode::Internal(i) => i.search(target),
            _ => panic!("Phantom node observed - what on earth did you do??"),
        }
    }

    fn insert(self, node: T) -> Self {
        let max_size = self.max_size();
        let mut s = self.0.write().unwrap();

        // replace the lock contents with garbage to get ownership in l
        let l = std::mem::replace(
            &mut *s,
            AnyNode::_PhantomData(PhantomData),
        );

	// replace the above garbage based on retrieved value
        *s = match l.insert(node) {
            MaybeSplit::Just(n) => n,
            MaybeSplit::Split(a, b) => AnyNode::Internal(SimpleInternalNode {
                children: vec![a, b],
                last: None,
                max_size,
                _pda: PhantomData,
                _pdb: PhantomData,
            }),
        };

        std::mem::drop(s);

        self
    }

    #[inline]
    fn max_size(&self) -> usize {
        (*self.0.write().unwrap()).max_size()
    }
}

impl<'a, T: Ord + Eq> InternalNode<'a, T> for Leaves<T> {
    fn empty(max: usize) -> Self {
        Self(Vec::new(), max)
    }

    fn search(&self, target: &T) -> Option<Rc<T>> {
        for x in self.0.iter().cloned() {
            if &*x == target {
                return Some(x.clone());
            }
        }
        None
    }

    fn insert(mut self, node: T) -> MaybeSplit<Self> {
        let node = Rc::new(node);

        match self.0.binary_search(&node) {
            Err(i) => self.0.insert(i, node),
            Ok(i) => self.0[i] = node,
        };

        let m = self.max_size();
        if self.0.len() > m {
            let a = Self(self.0.drain(0..(self.0.len() / 2)).collect(), m);
            let b = Self(self.0, m);

            MaybeSplit::Split(a, b)
        } else {
            MaybeSplit::Just(self)
        }
    }

    fn last(&self) -> Option<Rc<T>> {
        self.0.last().cloned()
    }

    fn max_size(&self) -> usize {
        self.1
    }
}

impl<'a, T: Ord + Eq> Leaf<'a, T> for Leaves<T> {
    fn from(el: T, max: usize) -> Self {
        Self(vec![Rc::new(el)], max)
    }
}

impl<'a, E: Ord + Eq, T: 'a + Leaf<'a, E>> InternalNode<'a, E>
    for SimpleInternalNode<'a, E, T>
{
    fn empty(max: usize) -> Self {
        Self {
            children: Vec::new(),
            last: None,
            max_size: max,
            _pda: PhantomData,
            _pdb: PhantomData,
        }
    }

    fn search(&self, target: &E) -> Option<Rc<E>> {
        self.children
            .iter()
            .find(|x| {
                let ir = x.last();
                ir.is_some() && target <= &*ir.unwrap()
            })
            .and_then(|x| x.search(target))
    }

    fn insert(mut self, node: E) -> MaybeSplit<Self> {
        let x = self.children.iter().enumerate().rev().find(|(_, x)| {
            let n = x.last();
            *(n.unwrap()) < node
        });

        // might be worth micro-optimizing this to avoid branching,
        // if possible. Will look at after benchmarking
        let i = match x {
            Some((i, _)) => i,
            None => 0,
        };

        let x = self.children.remove(i);
        match x.insert(node) {
            MaybeSplit::Just(l) => self.children.insert(i, l),
            MaybeSplit::Split(a, b) => {
                self.children.insert(i, a);
                self.children.insert(i + 1, b);
            }
        }

        let m = self.max_size();
        if self.children.len() > m {
            let mut a = Self {
                children: self
                    .children
                    .drain(0..(self.children.len() / 2 + 1))
                    .collect(),
                max_size: m,
                last: None,
                _pda: PhantomData,
                _pdb: PhantomData,
            };

            let mut b = Self {
                children: self.children,
                last: None,
                max_size: m,
                _pda: PhantomData,
                _pdb: PhantomData,
            };

            a.last = a.last().map(|x| x.clone());
            b.last = b.last().map(|x| x.clone());

            MaybeSplit::Split(a, b)
        } else {
            self.last = self.last().map(|x| x.clone());
            MaybeSplit::Just(self)
        }
    }

    fn last(&self) -> Option<Rc<E>> {
        self.last
            .clone()
            .or(self.children.last().and_then(|x| x.last()))
    }

    fn max_size(&self) -> usize {
        self.max_size
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn flat_tree<'a>() -> SimpleTree<'a, i32> {
        SimpleTree(RwLock::new(AnyNode::Internal(SimpleInternalNode {
            children: vec![AnyNode::Leaf(Leaves(
                vec![Rc::new(0), Rc::new(1), Rc::new(2), Rc::new(7)],
                4,
            ))],
            last: None,
            max_size: 4,
            _pda: PhantomData,
            _pdb: PhantomData,
        })))
    }

    fn two_three_tree<'a>() -> SimpleTree<'a, i32> {
        SimpleTree(RwLock::new(AnyNode::Internal(SimpleInternalNode {
            children: vec![
                AnyNode::Leaf(Leaves(vec![Rc::new(0), Rc::new(1)], 2)),
                AnyNode::Leaf(Leaves(vec![Rc::new(2), Rc::new(7)], 2)),
            ],
            last: None,
            max_size: 2,
            _pda: PhantomData,
            _pdb: PhantomData,
        })))
    }

    fn nested_two_three_tree<'a>() -> SimpleTree<'a, i32> {
        SimpleTree(RwLock::new(AnyNode::Internal(SimpleInternalNode {
            children: vec![
                AnyNode::Internal(SimpleInternalNode::<i32, Leaves<i32>> {
                    children: vec![
                        AnyNode::Leaf(Leaves(vec![Rc::new(1), Rc::new(2)], 2)),
                        AnyNode::Leaf(Leaves(vec![Rc::new(3), Rc::new(4)], 2)),
                    ],
                    last: None,
                    max_size: 2,
                    _pda: PhantomData,
                    _pdb: PhantomData,
                }),
                AnyNode::Internal(SimpleInternalNode::<i32, Leaves<i32>> {
                    children: vec![
                        AnyNode::Leaf(Leaves(vec![Rc::new(5), Rc::new(6)], 2)),
                        AnyNode::Leaf(Leaves(vec![Rc::new(7), Rc::new(8)], 2)),
                    ],
                    max_size: 2,
                    last: None,
                    _pda: PhantomData,
                    _pdb: PhantomData,
                }),
            ],
            last: None,
            max_size: 2,
            _pda: PhantomData,
            _pdb: PhantomData,
        })))
    }

    #[test]
    fn leaves_search() {
        let l = Leaves(vec![Rc::new(1), Rc::new(2), Rc::new(3)], 3);
        assert_eq!(l.search(&1), Some(Rc::new(1)));
        assert_eq!(l.search(&2), Some(Rc::new(2)));
        assert_eq!(l.search(&3), Some(Rc::new(3)));
    }

    #[test]
    fn flat_search() {
        let t = flat_tree();
        assert_eq!(t.search(&1), Some(Rc::new(1)));
        assert_eq!(t.search(&7), Some(Rc::new(7)));
        assert_eq!(t.search(&0), Some(Rc::new(0)));
        assert_eq!(t.search(&2), Some(Rc::new(2)));
        assert_eq!(t.search(&3), None);
    }

    #[test]
    fn flat_insert() {
        let l = flat_tree();
        let l = l.insert(4);
        assert_eq!(l.search(&4), Some(Rc::new(4)));
    }

    #[test]
    fn two_three_search() {
        let t = two_three_tree();
        assert_eq!(t.search(&1), Some(Rc::new(1)));
        assert_eq!(t.search(&7), Some(Rc::new(7)));
        assert_eq!(t.search(&0), Some(Rc::new(0)));
        assert_eq!(t.search(&2), Some(Rc::new(2)));
        assert_eq!(t.search(&3), None);
    }

    #[test]
    fn two_three_insert_greater() {
        let t = two_three_tree();
        let t = t.insert(8);
        assert_eq!(t.search(&8), Some(Rc::new(8)));
    }

    #[test]
    fn two_three_insert_less() {
        let t = two_three_tree();
        let t = t.insert(-1);
        assert_eq!(t.search(&(-1)), Some(Rc::new(-1)));
    }

    #[test]
    fn nested_two_three_insert() {
        let t = nested_two_three_tree();
        let t = t.insert(0);
        assert_eq!(t.search(&0), Some(Rc::new(0)));
    }

    #[test]
    fn nested_two_three_search() {
        let t = nested_two_three_tree();
        assert_eq!(t.search(&1), Some(Rc::new(1)));
        assert_eq!(t.search(&2), Some(Rc::new(2)));
        assert_eq!(t.search(&3), Some(Rc::new(3)));
        assert_eq!(t.search(&4), Some(Rc::new(4)));
        assert_eq!(t.search(&5), Some(Rc::new(5)));
        assert_eq!(t.search(&6), Some(Rc::new(6)));
        assert_eq!(t.search(&7), Some(Rc::new(7)));
        assert_eq!(t.search(&8), Some(Rc::new(8)));
    }
}
