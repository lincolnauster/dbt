//! This contains 'preset' implementations of the traits defined in module
//! `node_defs`.

use crate::node_defs::*;
use crate::util::*;
use std::marker::PhantomData;

pub mod simple;
