/// If a node *might* split, it'll return this. If it doesn't split it'll return Just itself,
/// otherwise the two nodes it'll be Split into.
pub enum MaybeSplit<T> {
    Just(T),
    Split(T, T),
}
