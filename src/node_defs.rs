//! This module contains traits necessary for tree handling. These are exposed
//! publicly in addition to being utilised by 'preset' nodes in the nodes
//! module.
use crate::util::MaybeSplit;
use std::marker::PhantomData;
use std::rc::Rc;

#[derive(Debug)]
pub enum AnyNode<'a, T: Ord + Eq, I: InternalNode<'a, T>, L: Leaf<'a, T>> {
    Leaf(L),
    Internal(I),
    /// Don't use this! This has no meaning, it's just for the
    /// compiler. Feel free to match with _ => panic!(...) when
    /// matching over this.
    _PhantomData(PhantomData<&'a T>),
}

impl<'a, T: Ord + Eq, I: InternalNode<'a, T>, L: Leaf<'a, T>>
    InternalNode<'a, T> for AnyNode<'a, T, I, L>
{
    fn empty(max: usize) -> Self {
        Self::Leaf(L::empty(max))
    }

    fn search(&self, target: &T) -> Option<Rc<T>> {
        match self {
            Self::Leaf(l) => l.search(target),
            Self::Internal(i) => i.search(target),
            _ => panic!(
                "Phantom node observed - something has gone horribly wrong."
            ),
        }
    }

    fn insert(self, node: T) -> MaybeSplit<Self> {
        match self {
            Self::Leaf(l) => match l.insert(node) {
                MaybeSplit::Just(a) => MaybeSplit::Just(Self::Leaf(a)),
                MaybeSplit::Split(a, b) => {
                    MaybeSplit::Split(Self::Leaf(a), Self::Leaf(b))
                }
            },
            Self::Internal(i) => match i.insert(node) {
                MaybeSplit::Just(a) => MaybeSplit::Just(Self::Internal(a)),
                MaybeSplit::Split(a, b) => {
                    MaybeSplit::Split(Self::Internal(a), Self::Internal(b))
                }
            },
            _ => panic!(
                "Phantom node observed - something has gone horribly wrong."
            ),
        }
    }

    fn last(&self) -> Option<Rc<T>> {
        match self {
            Self::Leaf(l) => l.last(),
            Self::Internal(i) => i.last(),
            _ => panic!(
                "Phantom node observed - something has gone horribly wrong."
            ),
        }
    }

    fn max_size(&self) -> usize {
	match self {
	    Self::Leaf(l) => l.max_size(),
	    Self::Internal(i) => i.max_size(),
	    _ => panic!(
		"Phantom node observed - something has gone horribly wrong."
	    ),
	}
    }
}

/// A root is generic and may be composed in any way.
pub trait Tree<'a, T: Ord + Eq> {   
    fn empty(max: usize) -> Self
    where
        Self: Sized;

    fn search(&self, target: &T) -> Option<Rc<T>>;
    fn insert(self, node: T) -> Self;

    fn max_size(&self) -> usize;
}

pub trait InternalNode<'a, T: Ord + Eq>
where
    Self: Sized,
{
    fn empty(max: usize) -> Self;

    fn search(&self, target: &T) -> Option<Rc<T>>;
    fn insert(self, node: T) -> MaybeSplit<Self>;
    fn last(&self) -> Option<Rc<T>>;

    fn max_size(&self) -> usize;
}

pub trait Leaf<'a, T: Ord + Eq>: InternalNode<'a, T>
where
    Self: Sized,
{
    fn from(el: T, max: usize) -> Self;
}
