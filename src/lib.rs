#![feature(test)]
extern crate test;

pub mod node_defs;
pub mod nodes;
pub mod util;

pub use node_defs::{InternalNode, Leaf, Tree};

#[cfg(test)]
mod tests {
    use super::*;
    use std::rc::Rc;
    use rand::{distributions::Uniform, Rng};
    use test::Bencher;

    #[test]
    fn additions() {
        let t = nodes::simple::SimpleTree::empty(2);
        assert_eq!(t.search(&4), None);
        assert_eq!(t.search(&5), None);
        let t = t.insert(4);
        let t = t.insert(5);
        let t = t.insert(-3);
        let t = t.insert(12);
        let t = t.insert(0);
        assert_eq!(t.search(&12), Some(Rc::new(12)));
        assert_eq!(t.search(&5), Some(Rc::new(5)));
        assert_eq!(t.search(&4), Some(Rc::new(4)));
        assert_eq!(t.search(&(-3)), Some(Rc::new(-3)));
        assert_eq!(t.search(&0), Some(Rc::new(0)));
        assert_eq!(t.search(&15), None);
    }

    /* in benchmarks, m = 4096, as that's a fairly standard page
    size. Currently, the insert algorithm is CPU-limited. */

    #[bench]
    fn sequential_insert(b: &mut Bencher) {
        b.iter(move || {
            let mut t = nodes::simple::SimpleTree::empty(1024);
            for i in 0..100_000 {
                t = t.insert(i);
            }
        });
    }

    #[bench]
    fn random_insert(b: &mut Bencher) {
        b.iter(|| {
            let r = Uniform::from(0..8191);
            let n = rand::thread_rng().sample_iter(&r).take(100_000);
            let mut t = nodes::simple::SimpleTree::empty(1024);
            for i in n {
                t = t.insert(i);
            }
        });
    }

    #[bench]
    fn sequential_read(b: &mut Bencher) {
	let mut t = nodes::simple::SimpleTree::empty(1024);
	for i in 0..8191 {
	    t = t.insert(i);
	}

	b.iter(|| {
	    for i in 0..8191 {
		assert_eq!(t.search(&i), Some(Rc::new(i)));
	    }
	});
    }

    #[bench]
    fn random_read(b: &mut Bencher) {
	let mut t = nodes::simple::SimpleTree::empty(1024);
	for i in rand::thread_rng().sample_iter(Uniform::from(0..8191)).take(100_000) {
	    t = t.insert(i);
	}

	b.iter(|| {
	    for i in 0..8191 {
		t.search(&i);
	    }
	});
    }
}
