# dbt
a dynamic b+tree implementation in Rust

## Design
This crate attempts to utilise the Rust type system to create a set of generic,
fast, and safe trees. This is achieved by defining a tree generically by its
polymorphic nodes. A tree is a wrapper of a record type (requiring Ord and Eq),
and node types for internal nodes and leaf nodes, each generic of the record
type R.

## Hacking
The definition of a node, child, and leaf is available as a trait in the
`node_defs` module. Preset implementations are available in the `nodes` module,
and each individual preset is a submodule of nodes.

## Usage
See rustdoc.

## TODOs
- [x] a balancing b-tree
- [x] cache `last` operation in simple tree
- [ ] b-tree using secondary storage
- [ ] parallelizing a b-tree
- [ ] distributing a b-tree
